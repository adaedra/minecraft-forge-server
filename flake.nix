{
    inputs = {
        nixpkgs.url = "nixpkgs";
    };

    outputs = { self, nixpkgs }: let
        system = "x86_64-linux";
        pkgs = import nixpkgs { inherit system; };

        minecraftVersion = "1.18.2";
        forgeVersion = "40.1.60";
    in rec {
        packages."${system}" = rec {
            minecraft = pkgs.fetchurl {
                url = "https://launcher.mojang.com/v1/objects/c8f83c5655308435b3dcf03c06d9fe8740a77469/server.jar";
                sha1 = "c8f83c5655308435b3dcf03c06d9fe8740a77469";
            };

            forgeInstaller = derivation {
                inherit system;

                name = "forge-installer";
                version = "${forgeVersion}+mc${minecraftVersion}";
                builder = "${pkgs.bash}/bin/bash";
                args = [ ./build-installer.sh ];

                installer = pkgs.fetchurl {
                    url = "https://maven.minecraftforge.net/net/minecraftforge/forge/${minecraftVersion}-${forgeVersion}/forge-${minecraftVersion}-${forgeVersion}-installer.jar";
                    sha1 = "892a7aadb61c6a2869c9ee02b553270c8fd71737";
                };
                SRC = ./.;
                PATH = with pkgs; lib.strings.makeBinPath [ coreutils zip unzip patch ];
            };

            forgeManifest = derivation {
                inherit system forgeInstaller;

                name = "forge-manifest";
                version = "${forgeVersion}+mc${minecraftVersion}";
                builder = "${pkgs.bash}/bin/bash";
                args = [ ./build-manifest.sh ];

                PATH = with pkgs; lib.strings.makeBinPath [ coreutils unzip ];
            };

            forgeLibraries = let
                manifest = (builtins.fromJSON (builtins.readFile "${forgeManifest}/version.json"));
                installManifest = (builtins.fromJSON (builtins.readFile "${forgeManifest}/install_profile.json"));
            in derivation {
                inherit system minecraft minecraftVersion;

                script = let
                    build-dependency = (dep: let
                        path = dep.downloads.artifact.path;
                        jar = pkgs.fetchurl { inherit (dep.downloads.artifact) url sha1; };
                    in ''
                        test -f $out/${path} || (
                            mkdir -p $out/${builtins.dirOf path}
                            cp ${jar} $out/${path}
                        )
                    '');
                in (builtins.concatStringsSep "" (map build-dependency (manifest.libraries ++ installManifest.libraries)));

                mappings = pkgs.fetchurl {
                    url = "https://launcher.mojang.com/v1/objects/e562f588fea155d96291267465dc3323bfe1551b/server.txt";
                    sha1 = "e562f588fea155d96291267465dc3323bfe1551b";
                };

                mcpVersion = (builtins.elemAt (builtins.split "'" installManifest.data.MCP_VERSION.server) 2);

                name = "forge-libraries";
                version = "${forgeVersion}+mc${minecraftVersion}";
                builder = "${pkgs.bash}/bin/bash";
                passAsFile = [ "script" ];
                args = [ ./build-libraries.sh ];

                PATH = with pkgs; lib.strings.makeBinPath [ coreutils ];
            };

            forgeServer = derivation {
                inherit system forgeInstaller forgeLibraries minecraftVersion forgeVersion;
                inherit (pkgs) bash;

                jdk = pkgs.jdk17_headless;

                name = "forge-server";
                version = "${forgeVersion}+mc${minecraftVersion}";
                builder = "${pkgs.bash}/bin/bash";
                args = [ ./build.sh ];

                PATH = with pkgs; lib.strings.makeBinPath [ coreutils gnused jdk ];
            };

            default = forgeServer;
        };

        devShells."${system}".default = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ unzip jq ];

            inherit (packages."${system}") forgeInstaller forgeLibraries;
            jdk = pkgs.jdk17_headless;
        };
    };
}
