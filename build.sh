set -e
set -u

source $jdk/nix-support/setup-hook

cp $forgeInstaller $TMP/forge-installer.jar

mkdir -p $out
cp -r $forgeLibraries $out/libraries
chmod -R u+w $out/libraries
(cd $TMP; java -jar forge-installer.jar --offline --installServer $out)
rm $out/{run.sh,run.bat,user_jvm_args.txt}

sed -e "s:libraries:${out}/libraries:g" < $out/libraries/net/minecraftforge/forge/${minecraftVersion}-${forgeVersion}/unix_args.txt > $out/unix_args.txt

mkdir -p $out/bin
cat <<EOF >${out}/bin/server
#! ${bash}/bin/bash

PATH=${jdk}/bin
source ${jdk}/nix-support/setup-hook

exec java -Xms2G -Xmx4G @${out}/unix_args.txt "\$@"
EOF
chmod +x $out/bin/server
