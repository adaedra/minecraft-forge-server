set -u
set -e

cp $installer $TMP/installer.jar
(
    cd $TMP
    unzip installer.jar install_profile.json
    patch < $SRC/install_profile.patch
    chmod u+w installer.jar
    zip installer.jar install_profile.json
    zip -d installer.jar 'META-INF/FORGE.SF'
)

cp $TMP/installer.jar $out
