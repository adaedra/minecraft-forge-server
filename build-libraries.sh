set -e
set -u

mkdir -p $out/net/minecraft/server/${minecraftVersion}
cp $minecraft $out/net/minecraft/server/${minecraftVersion}/server-${minecraftVersion}.jar

source $scriptPath

mkdir -p $out/net/minecraft/server/${minecraftVersion}-${mcpVersion}
cp $mappings $out/net/minecraft/server/${minecraftVersion}-${mcpVersion}/server-${minecraftVersion}-${mcpVersion}-mappings.txt
